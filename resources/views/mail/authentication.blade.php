<x-mail::message>
# Connexion à votre compte

Voici votre lien pour vous connecter.

<x-mail::button :url='$url'>
Se connecter
</x-mail::button>

</x-mail::message>
