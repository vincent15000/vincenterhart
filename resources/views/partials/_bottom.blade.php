<div class="flex justify-between items-center bg-gray-dark text-white px-8 lg:px-20 py-6">
    <span>Copyright © Prestidigitaweb 2020-{{ date('Y') }} - Tous droits de reproduction interdits</span>

    <a class="text-yellow" href="docs/cgu.pdf" target="_blank">CGU</a>
</div>
