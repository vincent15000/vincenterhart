<footer class="flex flex-col lg:flex-row lg:justify-between lg:items-center bg-gray-medium text-white px-8 lg:px-20 py-6 gap-8 lg:gap-0" id="contact">
    <div class="space-y-2">
        <p class="text-2xl font-bold mb-4">Propriétaire du site</p>
        <p>Erhart Vincent</p>
        <p>15000 Aurillac</p>
        <p><span class="text-yellow mr-2"><i class="fas fa-phone"></i></span>06 25 19 68 75</p>
        <p><a class="text-yellow" href="mailto:vincenterhart.formateur@gmail.com"><span class="text-yellow mr-2"><i class="fas fa-at"></i></span>vincenterhart.formateur@gmail.com</a></p>
    </div>

    <div class="space-y-2">
        <p class="text-2xl font-bold mb-4">Micro-entreprise</p>
        <p>EI Erhart Vincent</p>
        <p>SIRET : 510 350 150 00023 / APE : 8559A</p>
        <p>Déclaration d’activité numéro 84150323615 auprès du préfet de région Auvergne-Rhône-Alpes</p>
        <p>Assurance Hiscox : HSXPM310024158</p>
    </div>

    <div class="space-y-2">
        <p class="text-2xl font-bold mb-4">Hébergement du site</p>
        <p>IONOS</p>
        <p>7, place de la Gare</p>
        <p>57200 Sarreguemines</p>
        <p>Téléphone : 09 70 80 89 11</p>
    </div>
</footer>
