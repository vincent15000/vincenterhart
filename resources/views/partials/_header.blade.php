<header class="z-20 sticky top-0 h-20 lg:h-32 flex justify-between items-center bg-gray-dark text-white px-8 lg:px-20">
    <a
        class="font-signature text-yellow text-3xl lg:text-6xl"
        @if (Route::is('home'))
            href="#home"
        @else
            href="{{ route('home') }}"
            wire:navigate.hover
        @endif
    >
        P<span class="hidden lg:inline">resti</span> D<span class="hidden lg:inline">igita</span> W<span class="hidden lg:inline">eb</span>
    </a>

    <div
        x-data="{
            mobile: false,
            show: false,
            width: 0,
            getWidth() {
                return window.innerWidth > 0 ? window.innerWidth : screen.width;
            }
        }"
        x-on:resize.window="width = getWidth()"
        x-effect="mobile = width > 1024 ? false : true"
        x-effect="show = width > 1024 ? false : true"
        x-init="
            width = getWidth();
        "
        x-on:click="show = true"
        class="flex justify-center items-center"
    >
        <i class="fas fa-bars fa-2x" x-cloak x-show="mobile" x-on:click="show = !show" x-transition></i>

	    <nav class="flex flex-col border border-gray-light lg:border-0 absolute top-0 right-0 lg:relative px-8 py-6 lg:p-0 lg:flex-row lg:items-center gap-8 text-xl bg-gray-dark" x-cloak x-show="!mobile || show" x-transition x-on:click.outside="show = false">
	        <a
	            @class(['transition-all hover:text-yellow', 'text-yellow' => Route::is('home')])
	            @if (Route::is('home'))
	                href="#home"
	            @else
	                href="{{ route('home') }}"
	                wire:navigate.hover
	            @endif
	        >
	            Accueil
	        </a>

	        <a @class(['transition-all hover:text-yellow', 'text-yellow' => Route::is('trainings.*')]) href="{{ route('trainings.index') }}" wire:navigate.hover>Formations</a>

	        <a @class(['transition-all hover:text-yellow', 'text-yellow' => Route::is('applications.*')]) href="{{ route('applications.index') }}" wire:navigate.hover>Développement web</a>

	        @auth
	            <a @class(['transition-all hover:text-yellow', 'text-yellow' => Route::is('admin.dashboard')]) href="{{ route('admin.dashboard') }}" wire:navigate.hover>Tableau de bord</a>

	            <a class="transition-all hover:text-yellow" href="{{ route('admin.session.destroy') }}" wire:navigate.hover>Se déconnecter</a>
	        @endauth
	    </nav>
    </div>
</header>
