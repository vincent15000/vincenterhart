<section class="px-8 py-8 lg:px-20 space-y-8">
    <div class="flex items-center">
        <h1 class="text-3xl text-gray-dark">TECHNOLOGIES</h1>

        @auth
            <a class="ml-4 text-3xl text-gray-light hover:text-yellow transition-all" href="{{ route('admin.technologies.create') }}" wire:navigate><i class="fas fa-plus"></i></a>
        @endauth
    </div>

    <div class="flex items-center gap-4">
        @foreach ($technologies as $technology)
            <a href="{{ route('admin.technologies.edit', compact('technology')) }}" class="border border-gray-medium hover:bg-gray-medium hover:text-white px-4 py-2 rounded-full" wire:navigate>{{ $technology->name }}</a>
        @endforeach
    </div>
</section>
