<section class="min-h-[calc(100vh-8rem)] flex flex-col justify-center items-center gap-4 px-8">
    <form class="w-full lg:w-80 flex flex-grow flex-col gap-4 justify-center items-center" wire:submit="login()">
        <span class="text-yellow"><i class="fas fa-lock fa-3x"></i></span>

        <input wire:model="email" class="w-full outline-none px-4 py-2 border border-gray-light focus:border-yellow rounded-lg" placeholder="Adresse mail" type="text" />

        <button class="w-full px-4 py-2 border border-yellow hover:bg-yellow rounded-lg transition-all" type="submit">Se connecter</button>
    </form>
</section>
