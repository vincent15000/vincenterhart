<section class="flex flex-col justify-center items-center gap-4 px-8 py-8">
    <form class="w-full lg:w-2/3 flex flex-grow gap-8 justify-center items-center" wire:submit="save()">
        <livewire:components.upload-image-component wire:model="form.image" imageUrl="{{ $form->application->first_media_url }}"></livewire:components.upload-image-component>

        <div class="space-y-4">
            <div class="w-full flex flex-col gap-1">
                <x-label>Titre</x-label>
                <input wire:model.live="form.title" class="outline-none px-4 py-2 border border-gray-light focus:border-yellow rounded-lg" placeholder="Titre" type="text" />
                @error('form.title')
                    <span class="text-sm text-red-500">{{ $message }}</span>
                @enderror 
            </div>

            <div class="w-full flex flex-col gap-1">
                <x-label>Note</x-label>
                <input wire:model.live="form.note" class="outline-none px-4 py-2 border border-gray-light focus:border-yellow rounded-lg" placeholder="Note" type="text" />
                @error('form.note')
                    <span class="text-sm text-red-500">{{ $message }}</span>
                @enderror 
            </div>

            <div class="w-full flex flex-col gap-1">
                <x-label>Description</x-label>
                <textarea wire:model.live="form.description" class="outline-none px-4 py-2 border border-gray-light focus:border-yellow rounded-lg" placeholder="Description"></textarea>
                @error('form.description')
                    <span class="text-sm text-red-500">{{ $message }}</span>
                @enderror 
            </div>

            <div class="w-full flex flex-col gap-1">
                <x-label>URL</x-label>
                <input wire:model.live="form.url" class="outline-none px-4 py-2 border border-gray-light focus:border-yellow rounded-lg" placeholder="URL" type="text" />
                @error('form.url')
                    <span class="text-sm text-red-500">{{ $message }}</span>
                @enderror 
            </div>

            <div class="w-full flex flex-col gap-1">
                <x-label>Dépôt</x-label>
                <input wire:model.live="form.repository" class="outline-none px-4 py-2 border border-gray-light focus:border-yellow rounded-lg" placeholder="Dépôt" type="text" />
                @error('form.repository')
                    <span class="text-sm text-red-500">{{ $message }}</span>
                @enderror 
            </div>

            <div class="w-full flex flex-col gap-1">
                <x-label>Technologies</x-label>
                <div class="flex items-center gap-2 flex-wrap">
                    @foreach ($technologies as $technology)
                        <input type="checkbox" id="{{ 'technology-'.$technology->id }}" value="{{ $technology->name }}" wire:model.live="form.technologies" @checked(in_array($technology->name, $form->technologies)) class="hidden" />

                        <label @class([
                            'cursor-pointer transition-all rounded-lg border border-yellow rounded-lg text-sm px-4 py-2',
                            'bg-yellow' => in_array($technology->name, $form->technologies)
                        ]) for="{{ 'technology-'.$technology->id }}">{{ $technology->name }}</label>
                    @endforeach
                </div>
                @error('form.technologies')
                    <span class="text-sm text-red-500">{{ $message }}</span>
                @enderror 
            </div>

            <button class="w-full px-4 py-2 border border-yellow hover:bg-yellow rounded-lg transition-all" type="submit">Enregistrer</button>
        </div>
    </form>
</section>
