<div class="py-8 px-8 lg:px-20">
    {{-- <a href="{{ route('admin.trainings') }}">Formations</a> --}}
    <a href="{{ route('admin.applications') }}" wire:navigate>Applications</a>

    <a href="{{ route('admin.technologies') }}" wire:navigate>Technologies</a>
</div>
