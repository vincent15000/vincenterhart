<section class="flex flex-col justify-center items-center gap-4 px-8 py-8">
    <form class="w-full lg:w-2/3 flex flex-grow gap-8 justify-center items-center" wire:submit="save()">
        <div class="space-y-4">
            <div class="w-full flex flex-col gap-1">
                <x-label>Nom</x-label>
                <input wire:model.live="form.name" class="outline-none px-4 py-2 border border-gray-light focus:border-yellow rounded-lg" placeholder="Titre" type="text" />
                @error('form.name')
                    <span class="text-sm text-red-500">{{ $message }}</span>
                @enderror 
            </div>

            <button class="w-full px-4 py-2 border border-yellow hover:bg-yellow rounded-lg transition-all" type="submit">Enregistrer</button>
        </div>
    </form>
</section>
