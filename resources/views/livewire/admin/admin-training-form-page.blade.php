<section class="flex flex-col justify-center items-center gap-4 px-8 py-8">
    <form class="w-full lg:w-2/3 flex flex-grow gap-8 justify-center items-center" wire:submit="save()">
        <livewire:components.upload-image-component wire:model="form.image" imageUrl="{{ $form->training->first_media_url }}"></livewire:components.upload-image-component>

        <div class="space-y-4">
            <div class="w-full flex flex-col gap-1">
                <x-label>Titre</x-label>
                <input wire:model.live="form.title" class="outline-none px-4 py-2 border border-gray-light focus:border-yellow rounded-lg" placeholder="Titre" type="text" />
                @error('form.title')
                    <span class="text-sm text-red-500">{{ $message }}</span>
                @enderror 
            </div>

            <div class="w-full flex flex-col gap-1">
                <x-label>Sous-titre</x-label>
                <input wire:model.live="form.subtitle" class="outline-none px-4 py-2 border border-gray-light focus:border-yellow rounded-lg" placeholder="Sous-titre" type="text" />
                @error('form.subtitle')
                    <span class="text-sm text-red-500">{{ $message }}</span>
                @enderror 
            </div>

            <div class="w-full flex flex-col gap-1">
                <x-label>Accroche</x-label>
                <textarea wire:model.live="form.hook" class="outline-none px-4 py-2 border border-gray-light focus:border-yellow rounded-lg" placeholder="hook"></textarea>
                @error('form.hook')
                    <span class="text-sm text-red-500">{{ $message }}</span>
                @enderror 
            </div>

            <button class="w-full px-4 py-2 border border-yellow hover:bg-yellow rounded-lg transition-all" type="submit">Enregistrer</button>
        </div>
    </form>
</section>
