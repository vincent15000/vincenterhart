<div>
    <section class="min-h-[calc(100vh-8rem)] flex flex-col lg:flex-row justify-start px-8 lg:px-20 lg:justify-center items-center gap-8 lg:gap-20 text-center bg-home bg-no-repeat bg-cover text-white py-8">
        <div class="flex flex-col justify-center items-center gap-8">
            <img class="border-8 border-yellow rounded-full" src="{{ asset('images/vincenterhart.jpg') }}" alt="Vincent Erhart" />

            <div class="flex flex-col items-center gap-4">
                <span class="text-3xl text-white font-bold">Vincent Erhart</span>

                <span class="text-xl italic text-white">Formateur et développeur fullstack</span>

                <p class="text-base">06 25 19 68 75</p>
                <p class="text-base"><a class="text-yellow" href="mailto:vincenterhart.formateur@gmail.com">vincenterhart.formateur@gmail.com</a></p>
                <p class="text-3xl">
                    <a href="https://www.linkedin.com/in/vincent-erhart-89829115a" target="_blank"><i class="fa-brands fa-linkedin"></i></a>
                </p>
            </div>

            <a class="lg:hidden max-w-fit px-8 py-4 rounded-lg bg-yellow text-xl font-bold text-gray-dark" href="#services">PRESTATIONS</a>

            <a class="lg:hidden max-w-fit px-8 py-4 rounded-lg bg-yellow text-xl font-bold text-gray-dark" href="#testimonials">TEMOIGNAGES</a>
        </div>

        <div class="hidden lg:flex flex-col justify-center items-center gap-20 lg:gap-8">
            <span class="text-3xl italic">Découvrez la magie du web</span>

            <h1 class="text-6xl font-bold">PRESTI DIGITA WEB</h1>

            <span class="text-3xl italic">La magie, ça se prépare</span>

            <div class="flex flex-col lg:flex-row items-center gap-8">
                <a class="max-w-fit px-8 py-4 rounded-lg bg-yellow text-xl font-bold text-gray-dark" href="#services">PRESTATIONS</a>

                <a class="max-w-fit px-8 py-4 rounded-lg bg-yellow text-xl font-bold text-gray-dark" href="#testimonials">TEMOIGNAGES</a>
            </div>
        </div>
    </section>

    <section class="min-h-[calc(100vh-8rem)] flex flex-col justify-center items-center gap-8 text-center text-gray-dark px-8 lg:px-20 py-8 bg-gray-medium text-white" id="services">
        <h2 class="text-5xl font-bold">PRESTATIONS</h2>

        <span class="text-xl italic">Des formations et des applications web sur mesure</span>

        <div class="flex flex-col lg:flex-row justify-around gap-8 lg:gap-0">
            <x-service-card
                route="{{ route('trainings.index') }}"
                icon="fas fa-graduation-cap"
                title="Formations"
                description="Motivez vos apprenants avec des formations dynamiques et ludiques"
            ></x-service-card>

            <x-service-card
                route="{{ route('applications.index') }}"
                icon="fas fa-laptop-code"
                title="Développement web"
                description="Simplifiez la gestion de vos données professionnelles avec une application web sur mesure"
            ></x-service-card>
        </div>
    </section>

    <section class="min-h-[calc(100vh-8rem)] flex flex-col justify-center items-center gap-8 text-center text-gray-dark px-8 lg:px-20 py-8" id="testimonials">
        <h2 class="text-3xl text-gray-dark font-bold">ILS ME FONT CONFIANCE</h2>

        <article class="flex justify-around items-center flex-wrap gap-8">
            <div class="p-4 border border-gray-light shadow-lg rounded-lg bg-gray-medium">
                <img class="h-16" src="{{ asset('images/clients/doing.jpg') }}" />
            </div>

            <div class="p-4 border border-gray-light shadow-lg rounded-lg bg-white">
                <img class="h-16" src="{{ asset('images/clients/fdseo.jpg') }}" />
            </div>

            <div class="p-4 border border-gray-light shadow-lg rounded-lg bg-white">
                <img class="h-16" src="{{ asset('images/clients/lbpf.jpg') }}" />
            </div>

            <div class="p-4 border border-gray-light shadow-lg rounded-lg bg-white">
                <img class="h-16" src="{{ asset('images/clients/lyceesaintgeraud.jpg') }}" />
            </div>

            <div class="p-4 border border-gray-light shadow-lg rounded-lg bg-black">
                <img class="h-16" src="{{ asset('images/clients/nydvelos.jpg') }}" />
            </div>
        </article>

        <article class="w-full lg:w-1/2 flex flex-col text-justify gap-4">
            <div class="space-y-2">
                <span class="text-lg font-bold text-gray-dark">Jean-Sébastien Pin - Nyd-Vélos</span>

                <div class="text-gray-light space-y-1">
                    <p>
                        Excellente collaboration avec Vincent qui s'est chargé de développer des Features Back office, en PHP Laravel sur de l'existant. J'ai apprécié la capacité de Vincent à s'impliquer pleinement dans le projet. La volonté de comprendre des process métiers spécifiques. Les efforts et la recherche face aux imprévus.
                    </p>

                    <p>
                        Bref vous pouvez travailler avec Vincent en toute confiance. Dès lors qu'il est engagé dans un projet il va au bout.
                    </p>
                </div>
            </div>

            <div class="space-y-2">
                <span class="text-lg font-bold text-gray-dark">Frédéric Devaux - FDSEO</span>

                <div class="text-gray-light space-y-1">
                    <p>
                        Je recommande très fortement Vincent, non seulement pour ses qualités techniques, mais aussi pour ses qualités humaines.
                    </p>

                    <p>
                        Vincent est une personne très à l'écoute, il est bien organisé et gère très bien ses projets.
                    </p>
                    
                    <p>
                        Dans le cadre de mon projet, il a su rapidement s'adapter au code source existant pour développer les fonctionnalités demandées, un vrai sans faute !
                    </p>
                </div>
            </div>

            <div class="space-y-2">
                <span class="text-lg font-bold text-gray-dark">Karine Gaumat - Le Bon Plan Formation</span>

                <div class="text-gray-light space-y-1">
                    <p>
                        Vincent a su fait preuve d'écoute et de réactivité tout au long de notre collaboration. Il a réussi à comprendre mes besoins rapidement. Grâce à cela, j'ai à ce jour une application très fonctionnelle et très intuitive qui me permet de gagner beaucoup de temps.
                    </p>
                </div>
            </div>
        </article>
    </section>
</div>
