<div class="py-8 px-8 lg:px-20">
    <div class="flex items-center">
        <h1 class="text-3xl text-gray-dark">EXEMPLES DE FORMATION DISPENSEES</h1>

        @auth
            <a class="ml-4 text-3xl text-gray-light hover:text-yellow transition-all" href="{{ route('admin.trainings.create') }}" wire:navigate><i class="fas fa-plus"></i></a>
        @endauth
    </div>

    <div class="py-8 grid grid-cols-1 lg:grid-cols-2 gap-8">
        @foreach ($trainings as $training)
            <x-training-card :training="$training"></x-training-card>
        @endforeach
    </div>
</div>
