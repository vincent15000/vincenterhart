<div class="flex flex-col gap-2 items-center rounded-lg" x-data="{ files: null }">
    @if ($displayedImage)
        <div class="relative">
            <img class="w-80 h-96 rounded-lg object-cover border border-gray-light" src="{{ $displayedImage }}">

            <span wire:click="remove()" class="absolute -top-4 -right-4 text-red-500 bg-white cursor-pointer">
                <i class="fas fa-circle-xmark fa-2x"></i>
            </span>
        </div>
    @else
        <span class="transition-all text-yellow hover:scale-105 text-9xl cursor-pointer">
            <i class="fa-solid fa-image" @click="$refs.inputFile.click()"></i>
        </span>

        @error('image')
            <span>{{ $message }}</span>
        @enderror
    @endif

    <input wire:model="image" class="hidden" type="file" x-ref="inputFile" @change="files = Object.values($event.target.files)">

    <input type="hidden" name="deleteImage" value="{{ $deleteImage }}">
</div>
