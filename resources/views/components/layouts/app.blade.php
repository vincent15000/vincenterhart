<!DOCTYPE html>

<html class="scroll-smooth scroll-pt-20 lg:scroll-pt-32" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta property="og:title" content="Vincent Erhart, formateur et développeur web" />
        <meta property="og:image" content="https://www.vincenterhart.fr/images/vincenterhart.jpg" />
        <meta property="og:description" content="Formation en ludopédagogie et développement d'applications web sur mesure" />
        <meta property="og:url" content="https://www.vincenterhart.fr" />

        <title>Vincent Erhart, formateur en ludopédagogie et développeur web</title>
        
        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>

    <body class="w-full flex flex-col antialiased select-none" id="home">
        @include('partials._header')

        <main class="flex-1 overflow-auto">
            {{ $slot }}
        </main>

        @include('partials._footer')

        @include('partials._bottom')
    </body>
</html>
