<label class="text-sm text-gray-light italic">
    {{ $slot }}
</label>
