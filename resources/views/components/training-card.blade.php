@props([
    'training',
])

<article class="flex flex-col-reverse lg:flex-row justify-center gap-8 text-gray-dark lg:p-8 lg:hover:bg-gray-200 transition-all group">
    <div class="w-full lg:w-1/2 flex justify-end">
        <img class="w-full lg:h-96 object-cover" src="{{ $training->first_media_url }}" loading="lazy" />
    </div>

    <div class="w-full lg:w-1/2 flex flex-col lg:justify-between gap-4">
        <div class="flex flex-col gap-2">
            <h2 class="text-2xl font-bold flex justify-between">
                {{ $training->title }}

                @auth
                    <a class="text-gray-light hover:text-yellow transition-all" href="{{ route('admin.trainings.edit', compact('training')) }}" wire:navigate><i class="fas fa-edit"></i></a>
                @endauth
            </h2>

            @if ($training->subtitle)
                <p class="text-justify italic">{{ $training->subtitle }}</p>
            @endif

            @if ($training->hook)
                <p class="text-justify">{{ $training->hook }}</p>
            @endif
        </div>
    </div>
</article>
