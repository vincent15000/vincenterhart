@props([
    'route',
    'icon',
    'title',
    'description',
])

<div class="flex flex-col items-center gap-2 lg:gap-4">
    <a class="w-32 h-32 flex justify-center items-center transition-all duration-400 group hover:scale-110 cursor-pointer bg-yellow hover:bg-black rounded-full" href="{{ $route }}" wire:navigate>
        <span class="text-white group-hover:text-black group-hover:text-yellow transition-all duration-400 text-6xl">
            <i class="{{ $icon }}"></i>
        </span>
    </a>

    <span class="text-2xl font-bold">{{ $title }}</span>

    <span class="text-base max-w-[400px]">
        {{ $description }}
    </span>
</div>
