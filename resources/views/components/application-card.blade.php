@props([
    'application',
])

<article class="flex flex-col-reverse lg:flex-row justify-center gap-8 text-gray-dark lg:p-8 lg:hover:bg-gray-200 transition-all group">
    <div class="w-full lg:w-1/2 flex justify-end">
        <img class="w-full lg:h-96 object-cover" src="{{ $application->first_media_url }}" loading="lazy" />
    </div>

    <div class="w-full lg:w-1/2 flex flex-col lg:justify-between gap-4">
        <div class="flex flex-col gap-2">
            <h2 class="text-2xl font-bold flex justify-between">
                {{ $application->title }}

                @auth
                    <div class="space-x-4">
                        <a class="text-gray-light hover:text-yellow transition-all" href="{{ route('admin.applications.edit', compact('application')) }}" wire:navigate>
                            <i class="fas fa-edit"></i>
                        </a>
            
                        <button class="text-gray-light hover:text-yellow transition-all" wire:click.prevent="delete({{ $application->id }})">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>
                @endauth
            </h2>

            @if ($application->note)
                <p class="text-gray-light italic">({{ $application->note }})</p>
            @endif

            <p>{{ $application->description }}</p>

            @if ($application->url)
                <a href="{{ $application->url }}" target="_blank" class="text-gray-light hover:text-gray-medium transition-all">{{ $application->url }}</a>
            @endif

            @if ($application->repository)
                <a href="{{ $application->repository }}" target="_blank" class="text-gray-light hover:text-gray-medium transition-all">{{ $application->repository }}</a>
            @endif
        </div>

        <div class="flex flex-col gap-1 px-4 py-3 rounded-lg bg-gray-200 lg:group-hover:bg-white transition-all duration-1000">
            <h3 class="font-bold">Technologies utilisées</h3>
        
            <ul class="text-sm ml-8 list-disc">
                @foreach ($application->technologies as $technology)
                    <li>{{ $technology }}</li>
                @endforeach
            </ul>
        </div>
    </div>
</article>
