<?php

use App\Livewire\HomePage;
use App\Livewire\ContactPage;
use App\Livewire\TrainingsPage;
use App\Livewire\ApplicationsPage;
use Illuminate\Support\Facades\Route;
use App\Livewire\Admin\AdminLoginPage;
use App\Livewire\Admin\AdminDashboardPage;
use App\Livewire\Admin\AdminTrainingsPage;
use App\Http\Controllers\SessionController;
use App\Livewire\Admin\AdminApplicationsPage;
use App\Livewire\Admin\AdminTechnologiesPage;
use App\Livewire\Admin\AdminTrainingFormPage;
use App\Livewire\Admin\AdminTechnologyFormPage;
use App\Livewire\Admin\AdminApplicationFormPage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', HomePage::class)->name('home');

Route::get('applications', ApplicationsPage::class)->name('applications.index');

Route::get('trainings', TrainingsPage::class)->name('trainings.index');

Route::prefix('admin')->name('admin.')->group(function () {
	Route::get('login', AdminLoginPage::class)->name('login');
	Route::get('session/create', [SessionController::class, 'create'])->name('session.create');

	Route::middleware('auth')->group(function () {
		Route::get('dashboard', AdminDashboardPage::class)->name('dashboard');
		Route::get('session/destroy', [SessionController::class, 'destroy'])->name('session.destroy');

		Route::get('applications', AdminApplicationsPage::class)->name('applications');
		Route::get('applications/create', AdminApplicationFormPage::class)->name('applications.create');
		Route::get('applications/{application}/edit', AdminApplicationFormPage::class)->name('applications.edit');

		Route::get('technologies', AdminTechnologiesPage::class)->name('technologies');
		Route::get('technologies/create', AdminTechnologyFormPage::class)->name('technologies.create');
		Route::get('technologies/{technology}/edit', AdminTechnologyFormPage::class)->name('technologies.edit');

		Route::get('trainings', AdminTrainingsPage::class)->name('trainings');
		Route::get('trainings/create', AdminTrainingFormPage::class)->name('trainings.create');
		Route::get('trainings/{training}/edit', AdminTrainingFormPage::class)->name('trainings.edit');
	});
});
