/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
  ],
  safelist: [
    // background images
    'bg-home',
  ],
  theme: {
    extend: {
      colors: {
        gray: {
          light: '#6c757d',
          medium: '#495057',
          dark: '#212529',
        },
        yellow: '#ffc800',
      },
      backgroundImage: {
        'home': "url('/resources/img/fond.jpg')",
      },
    },
    fontFamily: {
      'signature': ['signature'],
    }
  },
  plugins: [],
}
