<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Application extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'title',
        'note',
        'description',
        'url',
        'repository',
        'technologies',
    ];

    protected $appends = [
        'first_media_url',
    ];

    protected $casts = [
        'technologies' => 'array',
    ];

    public function getFirstMediaUrlAttribute()
    {
        $media = $this->media->first();

        return $media ? $media->getFullUrl() : null;
    }
}
