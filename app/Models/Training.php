<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Training extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'title',
        'subtitle',
        'hook',
    ];

    protected $appends = [
        'first_media_url',
    ];

    public function getFirstMediaUrlAttribute()
    {
        $media = $this->media->first();

        return $media ? $media->getFullUrl() : null;
    }
}
