<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Application;
use Livewire\Attributes\On;
use App\Livewire\ApplicationsPage;

class ApplicationsPage extends Component
{
    public $applications;

    public function mount()
    {
        $this->getApplications();
    }

    #[On('application-deleted')]
    public function getApplications()
    {
        $this->applications = Application::with('media')->get();
    }

    public function delete(Application $application)
    {
        $application?->delete();

        $this->dispatch('application-deleted')->to(ApplicationsPage::class);
    }

    public function render()
    {
        return view('livewire.applications-page');
    }
}
