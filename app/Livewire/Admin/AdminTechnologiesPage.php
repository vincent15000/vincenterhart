<?php

namespace App\Livewire\Admin;

use Livewire\Component;
use App\Models\Technology;

class AdminTechnologiesPage extends Component
{
    public $technologies;

    public function mount()
    {
        $this->technologies = Technology::orderBy('name')->get();
    }

    public function render()
    {
        return view('livewire.admin.admin-technologies-page');
    }
}
