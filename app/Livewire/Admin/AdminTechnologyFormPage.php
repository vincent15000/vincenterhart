<?php

namespace App\Livewire\Admin;

use Livewire\Component;
use App\Models\Technology;
use App\Livewire\Forms\TechnologyForm;

class AdminTechnologyFormPage extends Component
{
    public TechnologyForm $form;
 
    public function mount(Technology $technology)
    {
        $this->form->set($technology);
    }
 
    public function save()
    {
        $this->form->save();
 
        return $this->redirectRoute('admin.technologies', navigate: true);
    }
 
    public function removeImage()
    {
        $this->form->image = null;
    }

    public function render()
    {
        return view('livewire.admin.admin-technology-form-page');
    }
}
