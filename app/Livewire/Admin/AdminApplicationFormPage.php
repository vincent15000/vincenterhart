<?php

namespace App\Livewire\Admin;

use App\Livewire\Forms\ApplicationForm;
use App\Models\Application;
use App\Models\Technology;
use Livewire\Attributes\On;
use Livewire\Component;

class AdminApplicationFormPage extends Component
{
    public ApplicationForm $form;
    public $technologies;
 
    public function mount(Application $application)
    {
        $this->form->set($application);
        $this->technologies = Technology::orderBy('name')->get();
    }
 
    public function save()
    {
        $this->form->save();
 
        return $this->redirectRoute('applications.index', navigate: true);
    }
 
    public function removeImage()
    {
        $this->form->image = null;
    }

    public function render()
    {
        return view('livewire.admin.admin-application-form-page');
    }
}
