<?php

namespace App\Livewire\Admin;

use Livewire\Component;
use App\Models\Training;
use App\Livewire\Forms\TrainingForm;

class AdminTrainingFormPage extends Component
{
    public TrainingForm $form;
 
    public function mount(Training $training)
    {
        $this->form->set($training);
    }
 
    public function save()
    {
        $this->form->save();
 
        return $this->redirectRoute('trainings.index', navigate: true);
    }
 
    public function removeImage()
    {
        $this->form->image = null;
    }

    public function render()
    {
        return view('livewire.admin.admin-training-form-page');
    }
}
