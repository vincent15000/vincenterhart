<?php

namespace App\Livewire\Admin;

use Livewire\Component;

class AdminApplicationsPage extends Component
{
    public function render()
    {
        return view('livewire.admin.admin-applications-page');
    }
}
