<?php

namespace App\Livewire\Admin;

use App\Models\User;
use App\Notifications\LoginRequestNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Livewire\Component;

class AdminLoginPage extends Component
{
    use Notifiable;

    public $email;

    public function login()
    {
        $email = Str::of($this->email)->trim()->toString();
        $user = User::where('email', $email)->first();

        if (!auth()->user() && $user) {
            $url = URL::temporarySignedRoute(
                'admin.session.create', now()->addMinutes(5), ['email' => $email]
            );

            $user->notify(new LoginRequestNotification($url));
        }

        $this->redirectRoute('home', navigate:true);
    }

    public function render()
    {
        return view('livewire.admin.admin-login-page');
    }
}
