<?php

namespace App\Livewire\Forms;

use Livewire\Form;
use App\Models\Training;
use Livewire\Attributes\Validate;
use Livewire\Features\SupportFileUploads\TemporaryUploadedFile;

class TrainingForm extends Form
{
    public ?Training $training;
 
    public $id = null;

    #[Validate('required', message: 'Le titre est obligatoire.')]
    public $title = '';
 
    #[Validate('required', message: 'Le sous-titre est obligatoire.')]
    public $subtitle = '';
 
    #[Validate('nullable')]
    public $hook = '';
 
    #[Validate('nullable')]
    #[Validate('image', message: 'Le fichier doit être une image au format jpg, jpeg, png, bmp, gif, svg ou webp.')]
    public $image = null;

    public function set(Training $training)
    {
        $training->load('media');

        $this->training = $training;
 
        $this->id = $training->id;
        $this->title = $training->title;
        $this->subtitle = $training->subtitle;
        $this->hook = $training->hook;
        $this->image = null;
    }

    public function save()
    {
        if ($this->id) {
            $this->update();
        } else {
            $this->store();
        }
    }

    public function store()
    {
        if ($this->image) {
            $this->image = TemporaryUploadedFile::unserializeFromLivewireRequest($this->image);
        }

        $this->validate();

        $training = Training::create($this->all());

        $training->addMedia($this->image)->preservingOriginal()->toMediaCollection('images', 'trainings');

        $this->reset();
    }
 
    public function update()
    {
        if ($this->image) {
            $this->image = TemporaryUploadedFile::unserializeFromLivewireRequest($this->image);
        }

        $this->validate();

        $this->training->update($this->all());

        if ($this->image) {
            $this->training->media->first()->delete();

            $this->training->addMedia($this->image)->preservingOriginal()->toMediaCollection('images', 'trainings');
        }

        $this->reset();
    }
}
