<?php

namespace App\Livewire\Forms;

use App\Models\Application;
use Livewire\Attributes\Validate;
use Livewire\Features\SupportFileUploads\TemporaryUploadedFile;
use Livewire\Form;

class ApplicationForm extends Form
{
    public ?Application $application;
 
    public $id = null;

    #[Validate('required', message: 'Le titre est obligatoire.')]
    public $title = '';
 
    #[Validate('nullable')]
    public $note = '';
 
    #[Validate('required', message: 'La description est obligatoire.')]
    public $description = '';
 
    #[Validate('nullable')]
    public $url = '';
 
    #[Validate('nullable')]
    public $repository = '';
 
    #[Validate('required', message: 'Au moins une technologie est obligatoire.')]
    public $technologies = [];
 
    #[Validate('nullable')]
    #[Validate('image', message: 'Le fichier doit être une image au format jpg, jpeg, png, bmp, gif, svg ou webp.')]
    public $image = null;

    public function set(Application $application)
    {
        $application->load('media');

        $this->application = $application;
 
        $this->id = $application->id;
        $this->title = $application->title;
        $this->note = $application->note;
        $this->description = $application->description;
        $this->url = $application->url;
        $this->repository = $application->repository;
        $this->technologies = $application->technologies ?? [];
        $this->image = null;
    }

    public function save()
    {
        if ($this->id) {
            $this->update();
        } else {
            $this->store();
        }
    }

    public function store()
    {
        if ($this->image !== null) {
            $this->image = TemporaryUploadedFile::unserializeFromLivewireRequest($this->image);
        }

        $this->validate();

        $application = Application::create($this->all());

        if ($this->image !== null) {
            $application->addMedia($this->image)->preservingOriginal()->toMediaCollection('images', 'applications');
        }

        $this->reset();
    }
 
    public function update()
    {
        if ($this->image) {
            $this->image = TemporaryUploadedFile::unserializeFromLivewireRequest($this->image);
        }

        $this->validate();

        $this->application->update($this->all());

        if ($this->image) {
            $this->application->media->first()?->delete();

            $this->application->addMedia($this->image)->preservingOriginal()->toMediaCollection('images', 'applications');
        }

        $this->reset();
    }
}
