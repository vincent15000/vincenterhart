<?php

namespace App\Livewire\Forms;

use Livewire\Form;
use App\Models\Technology;
use Livewire\Attributes\Validate;

class TechnologyForm extends Form
{
    public ?Technology $technology;
 
    public $id = null;

    #[Validate('required', message: 'Le nom est obligatoire.')]
    public $name = '';

    public function set(Technology $technology)
    {
        $this->technology = $technology;
 
        $this->id = $technology->id;
        $this->name = $technology->name;
    }

    public function save()
    {
        if ($this->id) {
            $this->update();
        } else {
            $this->store();
        }
    }

    public function store()
    {
        $this->validate();

        $technology = Technology::create($this->all());

        $this->reset();
    }
 
    public function update()
    {
        $this->validate();

        $this->technology->update($this->all());

        $this->reset();
    }
}
