<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Training;

class TrainingsPage extends Component
{
    public $trainings;

    public function mount()
    {
        $this->trainings = Training::with('media')->get();
    }

    public function render()
    {
        return view('livewire.trainings-page');
    }
}
