<?php

namespace App\Livewire\Components;

use Livewire\Attributes\Modelable;
use Livewire\Component;
use Livewire\WithFileUploads;

class UploadImageComponent extends Component
{
    use WithFileUploads;

    #[Modelable]
    public $image;

    public $imageUrl;
    public $displayedImage = null;
    public $deleteImage = false;

    public function mount()
    {
        $this->displayedImage = $this->imageUrl ? $this->imageUrl : null;
    }

    public function updatedImage()
    {
        $this->displayedImage = $this->image->temporaryUrl();
        $this->dispatch('image-updated', $this->image);
    }

    public function remove()
    {
        $this->deleteImage = true;
        $this->image = null;
        $this->displayedImage = null;
    }

    public function render()
    {
        return view('livewire.components.upload-image-component');
    }
}
